Workflow
==========

When you run `rye` the following things happen-

* Parse the config file
* Decide the list of tasks to run
* Make a copy of each task for each environment it should run in
* Make a list of all environments for these tasks
* Check each env- if the dependent files have changed, build it
* Run each command

All of the scheduled things (environment builds and tasks) are put onto
an event loop and start up in parallel as soon as their dependencies
finish. Say you have 3 tasks (A, B, C) running in 2 envs each (1, 2), and 1 of those envs need
to rebuild- the process would look like

A1 B1 C1 2
A2 A3 B2 B3 C2 C3
