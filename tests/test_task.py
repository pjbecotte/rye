from asyncio import get_event_loop
from collections import namedtuple
from unittest.mock import MagicMock, call, patch

from rye.task import Task


async def coro(*_, **__):
    pass


Mocks = namedtuple("Mocks", ["runner", "task"])


def run_with_mocks(commands, env_good=True):
    env = MagicMock()
    env.wait = coro
    env.is_good.return_value = env_good
    env.env.return_value = {"a": "42"}
    task = Task("mytask", env, commands, None)

    with patch.object(task, "run_command", side_effect=coro) as runner:
        get_event_loop().run_until_complete(task.run())
        return Mocks(runner, task)


def test_runs_commands():
    mocks = run_with_mocks([["cmd", "arg"], ["cmd2", "arg2"]])
    assert mocks.runner.mock_calls[0] == call(["cmd", "arg"], {"a": "42"})
    assert mocks.runner.mock_calls[1] == call(["cmd2", "arg2"], {"a": "42"})
    assert mocks.task.is_good()


def test_handles_single_str_command():
    mocks = run_with_mocks("cmd arg")
    assert mocks.runner.mock_calls[0] == call(["cmd", "arg"], {"a": "42"})
    assert mocks.task.is_good()


def test_handles_str_commands():
    mocks = run_with_mocks(["cmd arg", "cmd2 arg2"])
    assert mocks.runner.mock_calls[0] == call(["cmd", "arg"], {"a": "42"})
    assert mocks.runner.mock_calls[1] == call(["cmd2", "arg2"], {"a": "42"})
    assert mocks.task.is_good()


def test_exits_on_bad_env():
    mocks = run_with_mocks([["cmd", "arg"], ["cmd2", "arg2"]], False)
    assert mocks.runner.mock_calls == []
    assert not mocks.task.is_good()
