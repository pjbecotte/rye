import os
from asyncio import Lock, gather, get_event_loop, sleep

from rye.base_executable import Executable


def test_run_command():
    async def f():
        executable = Executable("my exec", should_print=False)
        await executable.run_command(["echo", "hello world"], {})
        return executable

    result = get_event_loop().run_until_complete(f())
    assert result.writer.messages == [
        "my exec > Executing ['echo', 'hello world']",
        "my exec > hello world",
    ]
    assert result.is_good()


def test_uses_env():
    async def f():
        executable = Executable("my exec", should_print=False)
        await executable.run_command(["env"], {"FAKEENV": "42"})
        return executable

    result = get_event_loop().run_until_complete(f())
    assert result.writer.messages == [
        "my exec > Executing ['env']",
        "my exec > FAKEENV=42",
    ]
    assert result.is_good()


def test_captures_failure():
    async def f():
        executable = Executable("my exec", should_print=False)
        await executable.run_command(["false"], {})
        return executable

    result = get_event_loop().run_until_complete(f())
    assert not result.is_good()


env = dict(os.environ.copy(), PYTHONUNBUFFERED="1")


async def first(func):
    await func(["bash", "-c", "echo 'A' && sleep .6 && echo 'B'"], env)


async def second(func):
    await sleep(0.01)
    await func(["bash", "-c", "sleep .4 && echo 'C'"], env)


def test_global_lock():
    async def f():
        lock = Lock()
        executable = Executable("", should_print=False, global_lock=lock)
        # Not sure if this is a good test- in a perfect world, this would do
        # A - sleep .2 - B - sleep .2 - C. However, startup times on the
        # subprocesses introduces some randomness. This is why .2 seconds instead
        # of .01. But on some systems, and occasionally, that could still be
        # not enough. The point of this is to prove that if the lock DIDNT work
        # (the next test), that test would fail.
        await gather(first(executable.run_locked), second(executable.run_locked))
        return executable

    result = get_event_loop().run_until_complete(f())
    assert [m for m in result.writer.messages if not m.startswith(" > Executing")] == [
        " > A",
        " > B",
        " > C",
    ]


def test_run_concurrent():
    async def f():
        executable = Executable("", should_print=False)
        await gather(first(executable.run_command), second(executable.run_command))
        return executable

    result = get_event_loop().run_until_complete(f())
    assert [m for m in result.writer.messages if not m.startswith(" > Executing")] == [
        " > A",
        " > C",
        " > B",
    ]
