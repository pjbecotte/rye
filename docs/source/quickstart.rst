Getting Started
=================

Install rye using pip

.. code-block:: bash

	pip install rye

Add rye config to your pyproject.toml file-

.. code-block:: toml

	# Global key values go here- default_tasks is the list of
	# tasks that will be run if you use `rye` with no sub-command
	[tool.rye]
	default_tasks = ["test", "lint", "format", "typing"]

	# Each task can have a config block where it specifies
	# the names of the environments it should run in. Each
	# task will run once for every environment in the list-
	# this will run the pytest task in the poetry.py37 and the
	# poetry.py36 environments.
	[tool.rye."task.pytest"]
	target_environments = ["poetry.py37", "poetry.py36"]

	# notice the quotes around "task.pytest" - that is one key
	# for rye, so needs quotes (toml would think it were a nested
	# dict otherwise)

	# Of course, sometimes we WANT a nested dict- in this case we
	# want a slightly different command to run in the python3.6 env
	# vs other environments
	[tool.rye."poetry.py36"."task.pytest"]
	commands = [["pytest", "tests", "--no-cov"]]

	# rye includes a number of default configurations such as task.pytest
	# and environment.poetry. We can leave alone any setting that we are
	# okay with the default value, or we can override them as we see fit
	[tool.rye."task.lint"]
	target_environments = ["poetry.py37"]
	# commands should be specified as lists of strings like you would in a
	# subprocess call or a Dockerfile. If you pass a string it'll try to
	# do the right thing, but no promises.
	commands = [["pylint", "src/rye", "tests"]]

	[tool.rye."task.format"]
	target_environments = ["poetry.py37"]
	# If you specify more then one command, they will be run in order.
	commands = [
		["black", ".", "--check"],
		["isort", "-rc", "-tc", "--check-only", "src", "tests"],
	]

	[tool.rye."task.typing"]
	target_environments = ["poetry.py37"]
	commands = [
		["mypy", "src/rye", "--ignore-missing-imports"],
	]

Once you have configured your toml file, you can use `rye` or `rye run`
to execute all of your tasks in parallel. If you wanted to just run one
task, you could use `rye run lint`.
