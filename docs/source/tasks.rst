Tasks
=======

A task is a series of commands to be run inside an environment.
An example whoule be `pytest tests` or `black .`. They can be
grouped together or broken apart at whatever granularity makes
sense. All tasks will attempt to be run in parallel. Each task
will run once for EACH environment it is set to target.

commands
---------

This is the list of commands to run for this task. Each command
should be a list of elements like ["python", "-m", "pytest"].
It will be run with the virtualenv from the environment activated.

isolate
--------

If this is `true`, this task will create its own copy of any
environments it runs in. Otherwise, an environment could potentially
be shared amongst many tasks.

target_environments
----------------------

This is a list of strings, specifying the names of the environments
that this should be run in. When the task is executed it will ensure
that all targeted environments are up to date before executing any
commands.

setup-local
--------------

There are two built-in tasks: `task.setup-local-poetry` and `task.setup-local-pip` that
target `environment#local` - these will run the `install` and `setup` commands against
the CURRENTLY configured virtualenv. They won't create or delete existing venvs, they
are to help you set up your IDE environment (of course, you could always point your IDE
at the .rye folder...)
