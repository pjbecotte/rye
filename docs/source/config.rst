Configuration
==================

Rye uses <settingscascade https://settingscascade.readthedocs.io/en/latest/>.
This means that config is set up as rule blocks with selectors, and you
can merge multiple selectors to build up the actual config that is applied.
Rye will look in the following locations-
# The environment variable RYE_FILE (path to the file)
# pyproject.toml
# rye.yaml
# rye.yml

It can parse both toml and yaml. In a toml file, data below the keey `tools.rye`
is used. In yaml files, the root is used.

For example-

.. code-block:: yaml

	task:
		target_environments: ["py37"]
	.pytest:
		commands: [["pytest", "tests"]]
	task.pytest:
		isolate: true

All three of those settings would apply to `task.pytest`, while only the first
would apply to `task.lint` and only the second to `environment.pytest`. The most
specific rule that applies will override anything less specific- so `task.pytest`
would overridde any common settings from `task` or `.pytest`.

Rye has a set of default configuration-

.. code-block:: python

    "current_working_directory": getcwd(),
    "task_class": "rye.task:Task",
    "env_class": "rye.environment:Env",
    "default_tasks": [],
    "base_location": ".rye",
    "task": {"isolate": False, "target_environments": [], "commands": []},
    "environment": {
        "location": ["{{ base_location }}", "{{ ('_', name, id) | join }}"],
        "clean_existing": True,
        "depends_files": [],
        "setup_commands": [["pip", "install", "-r", "requirements.txt"]],
        "create_command": [
            sys.executable,
            "-m",
            "virtualenv",
            "{{ location | list_to_path }}",
        ],
        "install_command": [],
        "required": True,
    },
    "environment.python37": {"python": "python3.7"},
    "environment.py37": {"python": "python3.7"},
    "environment.python36": {"python": "python3.6"},
    "environment.py36": {"python": "python3.6"},
    "environment.python38": {"python": "python3.8"},
    "environment.py38": {"python": "python3.8"},
    "environment.python27": {"python": "python2.7"},
    "environment.py27": {"python": "python2.7"},
    "environment.poetry": {
        "install_command": ["poetry", "install"],
        "depends_files": ["poetry.lock"],
        "setup_commands": [["poetry", "install", "--no-root"]],
    },
    "environment.pip": {
        "install_command": ["pip", "install", "-e", "."],
        "depends_files": ["requirements.txt"],
        "setup_commands": [["pip", "install", "-r", "requirements.txt"]],
    },
    "environment#local": {
        "location": ["{{ '' | current_venv }}"],
        "clean_existing": False,
        "python": "python",
    },
    "task.pytest": {"commands": [["pytest", "tests"]]},
    "task.setup-local-poetry": {
        "commands": [["echo", "SETUP COMPLETE"]],
        "target_environments": ["environment.poetry#local"],
    },
    "task.setup-local-pip": {
        "commands": [["echo", "SETUP COMPLETE"]],
        "target_environments": ["environment.pip#local"],
    },
