Config Filters
============

current_venv
-----------------

.. code-block:: yaml

	base_location: "{{ '' | current_venv}}"

Get the current venv (so you can use that instead of creating a new one)

home_dir
-----------------

.. code-block:: yaml

	base_location: "{{ '' | home_dir}}/myproject"

The equivalent of `~` on a unix system.


join
____________

join(sep, *args)

.. code-block:: yaml

	base_location: "{{',', 'a', 'b' | join}}"
    # "a,b"

Join a list of arguments into a string. The first arg is the seperator to use.

list_to_path
______________

.. code-block:: yaml

	base_location: "{{'tmp', 'dir' | list_to_path}}"
    # /tmp/dir

Turn a list of directories into a canonical path in a platform safe way.
