from pathlib import Path
from tempfile import TemporaryDirectory

from rye.application import Application
from rye.config import get_config


def test_integration():
    with TemporaryDirectory() as temp_dir:
        config = get_config(
            [
                {
                    "depends_files": [],
                    "isolate": False,
                    "python": None,
                    "create_command": ["echo", "CREATE"],
                    "setup_commands": [],
                    "install_command": [],
                    "default_tasks": ["a", "b", "fail", "fail2"],
                    "environment": {
                        "clean_existing": True,
                        "location": [temp_dir, "{{name}}"],
                        "required": True,
                    },
                    "task.a": {
                        "target_environments": ["c", "d"],
                        "commands": [["/bin/bash", "-c", "echo env=$VIRTUAL_ENV"]],
                    },
                    "task.b": {
                        "target_environments": ["c", "d"],
                        "commands": [["echo", "TASKB"]],
                    },
                    "task.fail": {
                        "target_environments": ["c"],
                        "commands": [["false"]],
                    },
                    "task.fail2": {
                        "target_environments": ["c"],
                        "commands": [["echo", "FAILED"], ["false"]],
                    },
                }
            ]
        )

        app = Application(config)
        bad = app.run()

        messages = {
            "c": [
                "ENV c > Preparing Env",
                "ENV c > Executing ['echo', 'CREATE']",
                "ENV c > CREATE",
                "ENV c > Env created",
            ],
            "d": [
                "ENV d > Preparing Env",
                "ENV d > Executing ['echo', 'CREATE']",
                "ENV d > CREATE",
                "ENV d > Env created",
            ],
            "TASK c#a": [
                "TASK c#a > Executing ['/bin/bash', '-c', 'echo env=$VIRTUAL_ENV']",
                f"TASK c#a > env={Path(temp_dir, 'c')}",
            ],
            "TASK c#b": ["TASK c#b > Executing ['echo', 'TASKB']", "TASK c#b > TASKB"],
            "TASK c#fail": ["TASK c#fail > Executing ['false']"],
            "TASK c#fail2": [
                "TASK c#fail2 > Executing ['echo', 'FAILED']",
                "TASK c#fail2 > FAILED",
                "TASK c#fail2 > Executing ['false']",
            ],
            "TASK d#a": [
                "TASK d#a > Executing ['/bin/bash', '-c', 'echo env=$VIRTUAL_ENV']",
                f"TASK d#a > env={Path(temp_dir, 'd')}",
            ],
            "TASK d#b": ["TASK d#b > Executing ['echo', 'TASKB']", "TASK d#b > TASKB"],
        }

        assert bad == 2
        for e in app.executables:
            assert e.writer.messages == messages[e.name]


def test_pick_envs():
    with TemporaryDirectory() as temp_dir:
        config = get_config(
            [
                {
                    "depends_files": [],
                    "isolate": False,
                    "python": None,
                    "create_command": ["echo", "CREATE"],
                    "setup_commands": [],
                    "install_command": [],
                    "default_tasks": ["a"],
                    "environment": {
                        "clean_existing": True,
                        "location": [temp_dir, "{{name}}"],
                        "required": True,
                    },
                    "task.a": {
                        "target_environments": ["c", "d"],
                        "commands": [["/bin/bash", "-c", "echo env=$VIRTUAL_ENV"]],
                    },
                }
            ]
        )

        app = Application(config)
        app.build_envs(["c"])

        messages = {
            "c": [
                "ENV c > Preparing Env",
                "ENV c > Executing ['echo', 'CREATE']",
                "ENV c > CREATE",
                "ENV c > Env created",
            ],
            "d": [],
            "TASK c#a": [],
            "TASK d#a": [],
        }
        for e in app.executables:
            assert e.writer.messages == messages[e.name]
