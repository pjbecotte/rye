Environments
=============

An environment in Rye lines up with a virtualenv. There are 3 sets
of commands that get run for each environment-

* create_command
* setup_commands
* install_command

Commands
----------

(All commands should be given as a list of strings like ["pytest", "tests"]

create_command
^^^^^^^^^^^^^^^^

This is the command to actually create the virtualenv. The default value
is `{{sys.executable}} -m virtualenv {{ location }}`. So given the location
of the environment, create a virtualenv there.

setup_commands
^^^^^^^^^^^^^^^^^

A list of commands to set up the environment. These will be run with the
virtualenv from create_command activated, so things like `pip install ...`
will work as expected.

install_command
^^^^^^^^^^^^^^^^^^

The command to actually install your project into your virtualenv. If your
code lives in a src directory, things like tests won't work at all without
doing this. It is seperated from `setup_commands` specifically so that you
can install your dependencies and your own code in seperate steps- which
can make it easier to build a docker container without having to re-download
the world every time you change your code. As an example workflow-

* RUN pip install -r requirements.txt
* COPY src ./src
* RUN pip install src

Configuration
---------------

Environments key configuration properties are the location to store them at
and the files to use to trigger dependency resolution. Given a location,
the virtualenv will be created at that location. In addition, a file will
be created in the parent directory for dependency files- the hash of the
contents of the dependency files. This will be the name of the location with
a . prepended. So if location is .rye/python36 -

.rye
	.python36 (dependency hash)
	python36 (virtualenv)
		bin
		lib....

location
^^^^^^^^^^^^

Should be a list of folder names (so that os path seperators can be handled correctly).
default is <base_location>/<env name>_<env_id

depends_files
^^^^^^^^^^^^^^^

Depends files is a list of filenames. The contents of all of these will
be hashed and stored. If the hashfile doesn't exist or the contents dont
match, the env will be recreated from scratch.

clean_existing
^^^^^^^^^^^^^^^^^^

If the depends files say we should rebuild and there is anything at the target location,
we remove it. This lets you have a completely clean build. If your command handles
incremental updates, set this and the target won't be removed.

required
^^^^^^^^^^^^^^^^^^

(default = True)
If required is False and the environment build fails, the tasks that depend on this env
will be skipped, but the build as a whole will not be marked as a failure. This is useful
to handle only running on available python installations, for example.
