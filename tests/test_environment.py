# pylint: disable=redefined-outer-name
from asyncio import get_event_loop
from collections import namedtuple
from hashlib import md5
from pathlib import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from unittest.mock import Mock, call, patch

import pytest

from rye.environment import Env


async def coro(*_, **__):
    pass


Mocks = namedtuple("Mocks", ["env", "runner", "lock_runner", "write"])


def run_with_mocks(env, rebuild=True, error=False):
    with patch("rye.environment.os.environ.copy", return_value={}), patch.object(
        env, "should_rebuild", return_value=rebuild
    ), patch.object(
        env, "env", return_value={"fake_env": "42"}
    ) as fake_env, patch.object(
        env, "run_command", side_effect=coro
    ) as runner, patch.object(
        env, "run_locked", side_effect=coro
    ) as lock_runner, patch.object(
        env, "write_hash"
    ) as write:

        if error:
            env.set_bad()
        get_event_loop().run_until_complete(env.run())
        return Mocks(fake_env, runner, lock_runner, write)


@pytest.fixture()
def mock_config():
    config = Mock()
    config.depends_files = []
    config.location = ["fakelocation"]
    config.python = None
    config.create_command = []
    config.setup_commands = []
    return config


def test_should_rebuild_location_not_exists(mock_config):
    env = Env("myenv", mock_config)
    assert env.should_rebuild()


def test_create_commands(mock_config):
    mock_config.create_command = ["create"]
    assert Env("myenv2", mock_config).create_command == ["create"]


def test_run_create_command(mock_config):
    mock_config.create_command = ["create"]
    env = Env("myenv", mock_config)
    mocks = run_with_mocks(env)
    assert mocks.runner.mock_calls[0] == call(["create"], {})
    mocks.write.assert_called_once()


def test_run_setup_commands(mock_config):
    mock_config.create_command = ["create"]
    mock_config.setup_commands = [["commanda", "arg"], ["commandb", "otherarg"]]
    env = Env("myenv", mock_config)
    mocks = run_with_mocks(env)
    assert mocks.runner.mock_calls[0] == call(["create"], {})
    assert mocks.runner.mock_calls[1] == call(["commanda", "arg"], {"fake_env": "42"})
    assert mocks.runner.mock_calls[2] == call(
        ["commandb", "otherarg"], {"fake_env": "42"}
    )


def test_run_locked(mock_config):
    mock_config.create_command = ["create"]
    mock_config.setup_commands = [["commanda", "arg"]]
    mock_config.install_command = ["install", "it"]
    env = Env("myenv", mock_config)
    mocks = run_with_mocks(env)
    assert mocks.runner.mock_calls[0] == call(["create"], {})
    assert mocks.runner.mock_calls[1] == call(["commanda", "arg"], {"fake_env": "42"})
    assert mocks.lock_runner.mock_calls[0] == call(
        ["install", "it"], {"fake_env": "42"}
    )


def test_skip_rebuild(mock_config):
    env = Env("myenv", mock_config)
    mocks = run_with_mocks(env, rebuild=False)
    assert mocks.runner.mock_calls == []
    assert env.done.is_set()


def test_cleans_on_error(mock_config):
    with TemporaryDirectory() as temp_dir:
        mock_config.location = [temp_dir, "myenv"]
        mock_config.create_command = ["create"]
        env = Env("myenv", mock_config)
        env.location.mkdir()
        mocks = run_with_mocks(env, error=True)
        assert mocks.runner.mock_calls[0] == call(["create"], {})
        assert not env.location.exists()


def test_does_not_clean_if_flag_set(mock_config):
    with TemporaryDirectory() as temp_dir:
        mock_config.location = [temp_dir, "myenv"]
        mock_config.setup_commands = [["install"]]
        mock_config.clean_existing = False
        env = Env("myenv", mock_config)
        env.location.mkdir()
        mocks = run_with_mocks(env, error=True)
        assert mocks.runner.mock_calls[0] == call(["install"], {"fake_env": "42"})
        assert env.location.exists()


def test_should_rebuild_hash_not_exists(mock_config):
    with TemporaryDirectory() as temp_dir:
        mock_config.location = [temp_dir, "myenv"]
        env = Env("myenv", mock_config)
        env.location.mkdir()
        assert env.location.exists()
        assert env.should_rebuild()
        assert not env.location.exists()


def test_should_rebuild_hash_wrong(mock_config):
    with TemporaryDirectory() as temp_dir:
        mock_config.location = [temp_dir, "myenv"]
        env = Env("myenv", mock_config)
        env.location.mkdir()
        assert env.location.exists()
        env.hashpath.write_bytes(b"ABCDE")
        assert env.should_rebuild()
        assert not env.location.exists()


def test_should_not_rebuild(mock_config):
    with TemporaryDirectory() as temp_dir, NamedTemporaryFile() as dep:
        mock_config.location = [temp_dir, "myenv"]
        mock_config.depends_files = [dep.name]
        env = Env("myenv", mock_config)
        env.location.mkdir()
        assert env.location.exists()
        env.hashpath.write_bytes(md5(b"ABCDE").digest())
        Path(dep.name).write_bytes(b"ABCDE")
        assert not env.should_rebuild()
        assert env.location.exists()


def test_respects_clean_existing(mock_config):
    with TemporaryDirectory() as temp_dir:
        mock_config.location = [temp_dir, "myenv"]
        mock_config.clean_existing = False
        env = Env("myenv", mock_config)
        env.location.mkdir()
        assert env.location.exists()
        env.hashpath.write_bytes(b"ABCDE")
        assert env.should_rebuild()
        assert env.location.exists()


def test_writes_hash_data(mock_config):
    with TemporaryDirectory() as temp_dir, NamedTemporaryFile() as dep:
        mock_config.location = [temp_dir, "myenv"]
        mock_config.depends_files = [dep.name]
        env = Env("myenv", mock_config)
        env.location.mkdir()
        assert env.location.exists()
        Path(dep.name).write_bytes(b"ABCDE")
        env.write_hash()
        assert not env.should_rebuild()
        assert env.location.exists()


def test_provides_env(mock_config):
    mock_config.location = ["envlocation"]
    env = Env("myenv", mock_config)
    path = Path(*mock_config.location).resolve()
    assert env.env()["PATH"].startswith(str(path))
    assert env.env()["VIRTUAL_ENV"] == str(path)
