import os
from os import getcwd
from pathlib import Path

import toml

from rye.application import Application
from rye.config import get_config
from rye.default_config import default

basepath = os.path.join("tests", "testenvs")

test_config_data = toml.loads(
    f"""
[tool.rye.environment]
depends_files = ["tests", "testenvs", "touch.txt"]

[tool.rye."environment.py37"]
python = "python3.7"

[tool.rye."environment.py36"]
python = "python3.6"

[tool.rye."task.mytask"]
target_environments = ["py36", "py37"]

[tool.rye]
default_tasks = ["mytask"]
base_location =  "{basepath}"
"""
)["tool"]["rye"]


def test_config():
    c = get_config([default, test_config_data])
    assert c.default_tasks == ["mytask"]
    assert c.task(name="mytask").target_environments == ["py36", "py37"]
    assert c.environment(name="py37").depends_files == [
        "tests",
        "testenvs",
        "touch.txt",
    ]
    assert c.environment(name="py37").location == [basepath, "py37"]


def test_app_task_list():
    app = Application(get_config([default, test_config_data]))
    assert {e.name for e in app.executables} == {
        "py36",
        "py37",
        "TASK py37#mytask",
        "TASK py36#mytask",
    }


def test_cwd():
    data = {"environment.basic": {"python": "{{current_working_directory}}"}}
    config = get_config([default, data])
    with config.context("environment.basic"):
        assert config.python == getcwd()


def test_home():
    data = {"environment.basic": {"python": "{{'' | home_dir}}"}}
    config = get_config([default, data])
    with config.context("environment.basic"):
        assert config.python == str(Path.home())
