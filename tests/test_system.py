from pathlib import Path
from unittest.mock import patch

import pytest
import yaml
from click.testing import CliRunner

from rye.__main__ import cli


@pytest.fixture(name="runner")
def _runner():
    runner = CliRunner()
    with runner.isolated_filesystem():
        yield runner


def make_config(data):
    Path("rye.yaml").write_text(yaml.safe_dump(data))


def test_default_command(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.echo": {
            "target_environments": ["fake_env"],
            "commands": [["echo", "hello world"]],
        },
        "environment.fake_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "The creation command"],
            "python": "python3.7",
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, [])
    assert result.exit_code == 0
    assert "The creation command" in result.output
    assert "hello world" in result.output


def test_run_command(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.echo": {
            "target_environments": ["fake_env"],
            "commands": [["echo", "hello world"]],
        },
        "task.dummy": {
            "target_environments": ["fake_env"],
            "commands": [["echo", "hello dummy"]],
        },
        "environment.fake_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "The creation command"],
        },
    }

    make_config(test_config)
    result = runner.invoke(cli, ["run", "dummy"])
    assert result.exit_code == 0
    assert "The creation command" in result.output
    assert "hello dummy" in result.output
    assert "hello world" not in result.output


def test_run_command_with_context(runner):
    test_config = """
    default_tasks:
        - echo
    .shadow:
        task.dummy:
            commands:
                - ["echo", "override shadow"]
    task.echo:
        target_environments:
            - fake_env
        commands:
            - ["echo", "hello world"]
    task.dummy:
        target_environments:
            - fake_env
        commands:
            - ["echo", "hello dummy"]
    environment.fake_env:
        depends_files: []
        setup_commands: []
        create_command: ["echo", "the creation command"]
    """
    Path("rye.yaml").write_text(test_config)
    result = runner.invoke(cli, ["-c", ".shadow", "run", "dummy"])
    assert result.exit_code == 0
    assert "the creation command" in result.output
    assert "override shadow" in result.output
    assert "hello world" not in result.output
    assert "hello dummy" not in result.output


def test_list_commands(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.echo": {
            "target_environments": ["fake_env"],
            "commands": [["echo", "hello world"]],
        },
        "task.dummy": {
            "target_environments": ["fake_env"],
            "commands": [["echo", "hello dummy"]],
        },
        "environment.fake_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "The creation command"],
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, ["list-tasks"])
    assert result.exit_code == 0
    assert "The creation command" not in result.output
    assert "echo" in result.output
    assert "dummy" in result.output


def test_multiple_envs(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.echo": {
            "target_environments": ["fake_env", "fake_env2"],
            "commands": [["echo", "hello world"]],
        },
        "environment.fake_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV1"],
        },
        "environment.fake_env2": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV2"],
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, ["run", "echo"])
    assert result.exit_code == 0
    assert result.output.count("hello world") == 4
    assert "MakingENV1" in result.output
    assert "MakingENV2" in result.output


def test_build_envs(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.echo": {
            "target_environments": ["fake_env", "fake_env2"],
            "commands": [["echo", "hello world"]],
        },
        "environment.fake_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV1"],
        },
        "environment.fake_env2": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV2"],
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, ["build-envs"])
    assert result.exit_code == 0
    assert result.output.count("hello world") == 0
    assert "MakingENV1" in result.output
    assert "MakingENV2" in result.output


def test_restrict_envs(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.echo": {
            "target_environments": ["fake_env", "fake_env2"],
            "commands": [["echo", "hello world"]],
        },
        "environment.fake_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV1"],
        },
        "environment.fake_env2": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV2"],
        },
    }

    make_config(test_config)
    result = runner.invoke(cli, ["run", "-e", "fake_env2", "echo"])
    assert result.exit_code == 0
    assert result.output.count("hello world") == 2
    assert "MakingENV1" not in result.output
    assert "MakingENV2" in result.output


def test_build_envs_with_filter(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.echo": {
            "target_environments": ["fake_env", "fake_env2"],
            "commands": [["echo", "hello world"]],
        },
        "environment.fake_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV1"],
        },
        "environment.fake_env2": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV2"],
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, ["build-envs", "fake_env2"])
    assert result.exit_code == 0
    assert result.output.count("hello world") == 0
    assert "MakingENV1" not in result.output
    assert "MakingENV2" in result.output


def test_specify_multiple_envs(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.echo": {
            "target_environments": ["fake_env", "fake_env2"],
            "commands": [["echo", "hello world"]],
        },
        "environment.fake_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV1"],
        },
        "environment.fake_env2": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["echo", "MakingENV2"],
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, ["run", "-e", "fake_env2,fake_env", "echo"])
    assert result.exit_code == 0
    assert result.output.count("hello world") == 4
    assert "MakingENV1" in result.output
    assert "MakingENV2" in result.output


def test_use_local(runner):
    test_config = {
        "default_tasks": ["echo"],
        "task.setup_local": {
            "target_environments": ["fake_env"],
            "commands": [["echo", "hello world"]],
        },
        "environment.fake_env": {
            "location": ["{{ '' | current_venv }}"],
            "clean_existing": False,
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["false"],
        },
    }

    make_config(test_config)
    result = runner.invoke(cli, ["run", "setup_local"])
    assert result.exit_code == 0
    assert result.output.count("hello world") == 2


def test_fails(runner):
    test_config = {
        "default_tasks": [],
        "task.should_fail": {
            "target_environments": ["fake_env"],
            "commands": [["echo", "hello world"], ["false"]],
        },
        "environment.fake_env": {
            "location": ["{{ '' | current_venv }}"],
            "depends_files": [],
            "setup_commands": [],
            "create_command": [],
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, ["run", "should_fail"])
    assert result.exit_code == 1
    assert result.output.count("hello world") == 2


def test_version(runner):
    test_config = {"tool": {"rye": {}}}
    make_config(test_config)
    with patch("rye.__main__.__version__", new="5.0.0"):
        result = runner.invoke(cli, ["-v"])
    assert result.exit_code == 0
    assert result.output.count("5.0.0") == 1


def test_skips_unrequired(runner):
    test_config = {
        "default_tasks": ["hello"],
        "task.hello": {
            "target_environments": ["good_env", "bad_env"],
            "commands": [["echo", "hello world"]],
        },
        "environment.good_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": [],
        },
        "environment.bad_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["false"],
            "required": False,
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, [])
    assert result.exit_code == 0
    assert result.output.count("hello world") == 2


def test_fails_if_required(runner):
    test_config = {
        "default_tasks": ["hello"],
        "task.hello": {
            "target_environments": ["good_env", "bad_env"],
            "commands": [["echo", "hello world"]],
        },
        "environment.good_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": [],
        },
        "environment.bad_env": {
            "depends_files": [],
            "setup_commands": [],
            "create_command": ["false"],
            "required": True,
        },
    }
    make_config(test_config)
    result = runner.invoke(cli, [])
    assert result.exit_code == 1
    assert result.output.count("hello world") == 2
