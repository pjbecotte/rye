.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Documentation

   quickstart
   workflow
   environments
   tasks
   commands
   config
