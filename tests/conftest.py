# pylint: disable=redefined-outer-name
import asyncio

import pytest


@pytest.fixture(autouse=True)
def loop():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    yield
    loop.close()
