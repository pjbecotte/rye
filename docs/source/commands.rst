Commands
============

`-c / --context`
-----------------

Using this will provide an extra context variable for this run. say you
had

.. code-block:: yaml

    .mycontext:
      task.mytask:
        location: /otherlocation

    task.mytask:
      location: /basiclocation

Running `rye run mytask` would use /basiclocation, but running `rye -c .mycontext run
mytask` would use /otherlocation instead.

run
-------

(if you call `rye` with no sub-command, `run` is the default)
`rye run [-e envs] [TASKS]`
Execute all the tasks in [TASKS]. If tasks is left empty, the
tasks specified by `default_tasks` will be executed.

If -e is specified, tasks will only be run in the specified environments.
If the given tasks arent configured with that env, nothing will be run.

example-
`rye run -e py37,py38 pytest`

build-envs
------------

`rye build-envs [ENVS]`
Build all the specified ENVS without running any tasks. Most
useful for something like building a docker testing image.
If ENVS is not specified, all the envs for all the tasks will
be built.

list-tasks
------------

`rye list-tasks`
This will output a list of all the tasks defined in the current config file.
